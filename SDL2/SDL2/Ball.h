#pragma once

#include "Sprite.h"

class Ball
{
public:
	int x = 0, y = 0;

	int vx = 0, vy = 0;


	float lastTimeStep = 0;
	float timeStep = 10.f;


	float speed = 1;
	SDL_Renderer* mRenderer;

	Sprite* mSprite;

	Ball(int x, int y, int vx,int vy, std::string spritePath, SDL_Renderer* mRenderer)
	{
		this->x = x;
		this->y = y;

		this->vx = vx;
		this->vy = vy;

		mSprite = new Sprite();

		mSprite->LoadFromFile(spritePath, mRenderer);
		this->mRenderer = mRenderer;
	}
	~Ball()
	{

	}

	void Draw()
	{
		Move();
		mSprite->Draw(x-mSprite->mWidth/2,y-mSprite->mHeight/2,mRenderer);
	}

private:

	void Move()
	{
		if (lastTimeStep < SDL_GetTicks())
		{
			lastTimeStep = SDL_GetTicks() + timeStep;
			x += vx * speed;
			y -= vy * speed;
		}
		

	}

};

