#include "View.h"
#include "Sprite.h"
#include "Controller.h"
#include "Mixer.h"
#include "Text.h"
#include <string>
#include <iostream>  
#include "Ball.h"
#include <SDL_mixer.h>


View::View(Controller* coontroller)
{
	controller = coontroller;
}

bool View::Init()
{
	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) >= 0)
	{

		mWindow = SDL_CreateWindow("Mi Juego", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
		//WINDOW
		if (mWindow == nullptr)
		{
			std::cout << "No cargo Ventana" << std::endl;
			return false;
		}
		//RENDERER

		mRenderer = SDL_CreateRenderer(mWindow, -1, SDL_RENDERER_ACCELERATED);

		if (mRenderer == nullptr)
		{
			std::cout << "No cargo el Renderer" << std::endl;
			return false;
		}
		
		int img_Flag = IMG_INIT_PNG;

		if (IMG_Init(img_Flag) == false)
		{
			std::cout << "No cargo la lib de PNG"<<std::endl;
		}

		
		score = new Text();

		score2 = new Text();

		
		
		

		return true;
	}
	else
	{
		return false;
	}

}

void View::Start()
 {
	
	player = new Sprite();

	player->LoadFromFile("Pong.png", mRenderer);

	
	
	AI = new Sprite();

	AI->LoadFromFile("Pong.png", mRenderer);
	ball = new Ball(SCREEN_WIDTH /2, SCREEN_HEIGHT / 2, 2, 2, "Ball.png", mRenderer);

	



	
 }

void View::Draw()
{
	score->LoadFromRendererText(std::to_string(controller->score), { 255,255,255 }, mRenderer);
	score2->LoadFromRendererText(std::to_string(controller->score2), { 255,255,255 }, mRenderer);

	SDL_SetRenderDrawColor(mRenderer, 0, 0, 0, 255);

	SDL_RenderClear(mRenderer);

	
	
	player->Draw(32 - (player->mHeight/2), controller->y - (player->mHeight/2),mRenderer, NULL, 0, { 0 }, SDL_FLIP_NONE);

	AI->Draw((SCREEN_WIDTH  - 32) - (AI->mWidth/2), controller->y2 - (AI->mHeight/2), mRenderer, NULL, 0, { 0 }, SDL_FLIP_NONE);

	
	ball->Draw();

	score->Draw(SCREEN_WIDTH - SCREEN_WIDTH / 4, 20 , mRenderer);
	score2->Draw(SCREEN_WIDTH / 4, 20, mRenderer);

	

	
	if (ball->y >SCREEN_HEIGHT - 32)
	{
		if( ball->vy < 0)
			ball->vy *= -1;
	}

	if (ball->x >= SCREEN_WIDTH - 32)
	{
		controller->score2 += 1;
		ball->x = SCREEN_WIDTH / 2 -16;
		ball->y = SCREEN_HEIGHT / 2 -16;

		ball->vx *= -1;
	}

	if (ball->y < 0)
	{
		if( ball->vy > 0)
			ball->vy *= -1;
	}
	if (ball->x <=0)
	{
		controller->score += 1;
		ball->x = SCREEN_WIDTH / 2;
		ball->y = SCREEN_HEIGHT / 2;

		ball->vx *= - 1;
		
	}

	
	
	if (ball->y > player->y - player->mHeight/2 && ball->y < player->y + player->mHeight / 2)
	{
		if (ball->x - ball->mSprite->mWidth <= player->x + player->mWidth / 2 && ball->x - ball->mSprite->mWidth > player->x - player->mWidth / 2)
		{
			if(ball->vx < 0)
				ball->vx *= -1;
		}
			
	}

	


	if (ball->y > AI->y - AI->mHeight / 2 && ball->y < AI->y + AI->mHeight / 2)
	{
		if (ball->x + ball->mSprite->mWidth >= AI->x - AI->mWidth / 2 && ball->x + ball->mSprite->mWidth < AI->x + AI->mWidth / 2)
		{
			if (ball->vx > 0)
				ball->vx *= -1;
		}

	}

	

	
	 SDL_RenderPresent(mRenderer);
}

