#include <iostream>
#include <SDL.h>
#include <string>
#include "Controller.h"

#include "View.h"


int main(int argc, char* argv[])
{
	bool isGameOver = false;


	Controller* controller = new Controller();
	View* view = new View(controller);


	view->Init();
	view->Start();

	while (isGameOver == false)
	{
		if (controller->CheckSDLEvent())
		{
			controller->ReadInputs(&isGameOver);
		}
		view->Draw();
	}


	return 0;
}