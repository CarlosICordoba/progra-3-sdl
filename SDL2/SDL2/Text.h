#pragma once
#include <SDL.h>
#include <SDL_image.h>
#include <string>
#include <SDL_ttf.h>
#include <iostream>


class Text
{

public:

	//Imagen generada con el texto;
	SDL_Texture* mTexture;
	TTF_Font* font;

	

	Text()
	{
		if (TTF_WasInit > 0 && TTF_Init() == -1)
		{
			std::cout << "No se pudo inicializar libreria de fonts :( " << TTF_GetError() << std::endl;
		}

		font = TTF_OpenFont("font.ttf",80);

		if (font == NULL)
		{
			std::cout << "No se pudo inicializar la font :( " << TTF_GetError() << std::endl;
		}
	}
	~Text()
	{

	}

	bool LoadFromRendererText(std::string text,SDL_Color textColor, SDL_Renderer* renderer)
	{
		//Antes de crear un  nuevo texto,liberamos el anterior
		FreeTexture();

		//Creo una surface de pixeles con el texto
		SDL_Surface* textSurface = TTF_RenderText_Solid(font, text.c_str(), textColor);

		if (textSurface == NULL)
		{
			std::cout << "No se pudo cargar la surface del texto :( " << TTF_GetError() << std::endl;

			return false;
		}

		mTexture = SDL_CreateTextureFromSurface(renderer, textSurface);

		if (mTexture == NULL)
		{
			std::cout << "No se pudo cargar la textura del texto :( " << TTF_GetError() << std::endl;
			
		}
		else
		{

			mWidth = textSurface->w;
			mHeight = textSurface->h;
		}

		SDL_FreeSurface(textSurface);

		return mTexture != NULL;


	}

	void FreeTexture()
	{
		if (mTexture != NULL)
		{
			SDL_DestroyTexture(mTexture);
			mTexture = NULL;
			mWidth = 0;
			mHeight = 0;
		}
	}

	void Draw(int x, int y ,SDL_Renderer* renderer, SDL_Rect* clip = NULL, double angle = 0.0, SDL_Point * center = NULL, SDL_RendererFlip flip = SDL_FLIP_NONE)
	{
		SDL_Rect renderQuad = { x,y,mWidth,mHeight };

		if (clip != NULL)
		{
			renderQuad.w = clip->w;
			renderQuad.h = clip->h;
		}


		SDL_RenderCopyEx(renderer, mTexture, NULL, &renderQuad, angle, center, flip);
	}
private:

	int mWidth, mHeight;

};

