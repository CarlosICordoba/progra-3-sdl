#pragma once

#include <SDL.h>
#include <vector>
#include <math.h>

#define PI 3.14159265



class Circule
{
	
public:
	SDL_Renderer* mRenderer = nullptr;
	SDL_Point centro = {0,0};
	std::vector<SDL_Point> circle;
	int radio = 0.0;

	Circule(SDL_Point Centro,int Radio,SDL_Renderer* Renderer)
	{
		centro = Centro;
		radio = Radio;
		mRenderer = Renderer;
		
		for (int i = 0; i < 361; i++)
		{
			SDL_Point punto =  {(radio * (int)(cos(i) + centro.x)),(radio * (int)(sin(i)+ centro.y))};

			circle.push_back(punto);


		}
		
	}


	~Circule()
	{

	}

	void Draw()
	{
		SDL_SetRenderDrawColor(mRenderer, 255, 255, 255, SDL_ALPHA_OPAQUE);
		

		for (int i = 0; i < 360; i++)
		{
			SDL_RenderDrawPoint(mRenderer, circle[i].x, circle[i].y);
			SDL_RenderPresent(mRenderer);

		}
	}

private:

};

