#pragma once
#include <SDL.h>

class PrimitiveRect
{
public:
	SDL_Renderer* mRenderer;

	SDL_Rect  mRectData;
	int r, g, b, a;

	PrimitiveRect(SDL_Renderer* renderer,SDL_Rect data,int R = 255,int G = 255, int B = 255 , int A = 255 )
	{
		mRenderer = renderer;
		mRectData = data;
		r = R; g = G; b = B; a = A;

	}
	~PrimitiveRect() 
	{

	}

	void Draw()
	{
		SDL_SetRenderDrawColor(mRenderer, r, g, b, a);
		SDL_RenderFillRect(mRenderer, &mRectData);
	}

private:

};

