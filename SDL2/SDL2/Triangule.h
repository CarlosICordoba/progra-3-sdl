#pragma once
#include <SDL.h>

class Triangule
{
public:
	SDL_Renderer* mRenderer;

	SDL_Point pointA;
	SDL_Point pointB;
	SDL_Point pointC;


	
	Triangule(SDL_Renderer* renderer,SDL_Point A,SDL_Point B, SDL_Point C)
	{
		mRenderer = renderer;
		pointA = A;
		pointB = B;
		pointC = C;

	}
	~Triangule()
	{
		
	}

	void Draw()
	{
		SDL_SetRenderDrawColor(mRenderer, 255, 255, 255, SDL_ALPHA_OPAQUE);
		SDL_RenderDrawLine(mRenderer, pointA.x, pointA.y, pointB.x, pointB.y);
		SDL_RenderDrawLine(mRenderer, pointB.x, pointB.y, pointC.x, pointC.y);
		SDL_RenderDrawLine(mRenderer, pointC.x, pointC.y, pointA.x, pointA.y);
		SDL_RenderPresent(mRenderer);
	}

private:

};

