#pragma once
#include <SDL.h>
#include <iostream>
#include <SDL_mixer.h>
#include "View.h"

class Mixer;
class View;
class Sprite;


class Controller
{
public:
	int mouseX, mouseY;

	bool CheckSDLEvent();
	void ReadInputs(bool* Exit);

	void OnMousePressed(int x, int y);
	void OnKeyDown(SDL_Scancode code);

	void OnMouseMoved();
	
	Mixer* mMixer = nullptr;
	View* view = nullptr;
	Sprite* sprite = nullptr;

	int x;
	int y = 360;

	int y2 = 360;

	int speed = 10;

	bool wIsPressed;
	bool sIsPressed;

	int score;
	int score2;


private:
	
	SDL_Event mEvent;
};
