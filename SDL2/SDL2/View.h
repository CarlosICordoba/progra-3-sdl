#pragma once
#include <SDL.h>
#include <iostream>
#include <vector>

class Controller;
class Sprite;
class Mixer;
class Text;
class Ball;

class View
{
private:
	const int SCREEN_WIDTH = 1080;
	const int SCREEN_HEIGHT = 720;

	SDL_Window* mWindow = nullptr;
	SDL_Renderer* mRenderer = nullptr;



public:
	Controller* controller;
	
	Sprite* player = nullptr;
	Sprite* AI = nullptr;
	View(Controller* coontroller);
	Text* text = nullptr;

	Mixer* mMixer = nullptr;

	Text* score = nullptr;
	Text* score2 = nullptr;

	Ball* ball = nullptr;


	

	bool Init();
	
	void Start();
	void Draw();


};