#pragma once

#include <iostream>
#include <SDL.h>
#include <SDL_image.h>


class Sprite
{
public:	
	Sprite()
	{
		
		mTexture = nullptr;
		mWidth = 0;
		mHeight = 0;
	}
	~Sprite()
	{
		Free();
	}

	bool LoadFromFile(std::string path,SDL_Renderer* mRenderer)
	{
		
		
		SDL_Surface* loadSurface = IMG_Load(path.c_str());

		if (loadSurface == NULL)
		{
			std::cout << "No se cargo la imagen" << path << std::endl;
			return false;

		}
		else
		{
			SDL_SetColorKey(loadSurface, SDL_TRUE, SDL_MapRGB(loadSurface->format, 0, 0xFF, 0xFF));

			mTexture = SDL_CreateTextureFromSurface(mRenderer, loadSurface);

			mWidth = loadSurface->w;
			mHeight = loadSurface->h;

		
		}
		SDL_FreeSurface(loadSurface);

		return mTexture != NULL;
	}
	void Free()
	{
		if (mTexture != NULL)
		{
			SDL_DestroyTexture(mTexture);
			mWidth = 0;
			mHeight = 0;
			mTexture = NULL;
		}
	}

	void SetColor(Uint8 red, Uint8 green, Uint8 blue)
	{
		SDL_SetTextureColorMod(mTexture, red, green, blue);
	}
	void SetBlendMode(SDL_BlendMode blending)
	{
		SDL_SetTextureBlendMode(mTexture, blending);
	}
	void SetAlpha(Uint8 alpha) 
	{
		SDL_SetTextureAlphaMod(mTexture, alpha);
	}
	void Draw(int x,int y,SDL_Renderer* renderer, SDL_Rect* clip = NULL, double angle = 0.0, SDL_Point * center = NULL, SDL_RendererFlip flip = SDL_FLIP_NONE)
	{
		this->x = x;
		this->y = y;


		SDL_Rect renderQuad = { x,y,mWidth,mHeight };

		if (clip != NULL)
		{
			renderQuad.w = clip->w;
			renderQuad.h = clip->h;
		}
		

		SDL_RenderCopyEx(renderer, mTexture, NULL, &renderQuad, angle, center, flip);
	}
	
	int x;
	int y;
	int mWidth, mHeight;

private:

	SDL_Texture* mTexture;
	
	
	
	
};
